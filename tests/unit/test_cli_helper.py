""" Testing the different helper function used in the cli
"""
import string
import os
import shutil

from lemmings.cli_main import (
    custom_name,
    remove_files_folders
)

def test_custom_name():
    """ Ensure correct structure of folder naming
    """
    job_name = custom_name()
    job_name = list(job_name)
    vowels = list("AEIOU")
    consonants = list(set(string.ascii_uppercase) - set(vowels))
    number = list("0123456789")

    assert job_name[0] in consonants
    assert job_name[2] in consonants
    assert job_name[1] in vowels
    assert job_name[3] in vowels
    assert job_name[4] and job_name[5] in number


# def test_get_workflow_py(datadir):
#     """ Cheking correct behavior in finding {workflows}.py file(s)

#         Currently not check for LEMMINGS_WORKFLOWS env variable as
#         its use is not yet advocated.
#         Also, no check of pre-def workflows in chain/ or /workflows folders.
#     """

#     current_dir = os.getcwd()
#     os.chdir(datadir)
#     workflow = 'fake_workflow'
#     try:
#         _get_workflow_py(workflow)
#         raise ValueError(f"Something went wrong in checking '{workflow}'.py")
#     except EnvironmentError as excep:
#         msg = f"Your specified workflow '{ workflow}' doesn't exist in  current directory\n"
#         assert msg  in str(excep)
       
#     assert _get_workflow_py('empty_workflow').split('/')[-1] == 'empty_workflow.py'

#     os.chdir(current_dir)


# def test_get_workflow_yml(datadir):
#     """ Cheking correct behavior in finding {workflows}.yml file
#     """

#     current_dir = os.getcwd()
#     os.chdir(datadir)
#     try:
#         _get_workflow_yml('fake_workflow')
#         raise ValueError("Something went wrong in checking 'fake_workflow'.yml")
#     except FileNotFoundError:
#         pass

#     assert _get_workflow_yml('empty_workflow').split('/')[-1] == 'empty_workflow.yml'

#     os.mkdir('Test')
#     os.chdir('Test')
#     assert _get_workflow_yml('../empty_workflow').split('/')[-1] == 'empty_workflow.yml'
#     assert _get_workflow_yml('../empty_workflow',user_yaml = True).split('/')[-1] == 'empty_workflow.yml'
#     try:
#         _get_workflow_yml('../fake_workflow',user_yaml = True)
#         raise ValueError("Something went wrong in checking 'fake_workflow'.yml")
#     except FileNotFoundError:
#         pass


#     os.chdir(current_dir)


def test_remove_files_folders(datadir):
    """ Testing removal of files and folders
    """

    current_dir = os.getcwd()
    os.chdir(datadir)
    os.makedirs('TEST/FOLDER')
    # os.mknod("newfile.txt")
    shutil.copy('empty_workflow.py','TEST/empty_workflow.py')
    shutil.copy('database.yml','TEST/FOLDER/database.yml')
    shutil.copytree('TEST/FOLDER','TEST/FOLDER2')
    assert sorted(os.listdir('TEST')) == sorted(['empty_workflow.py', 'FOLDER', 'FOLDER2'])
    assert os.listdir('TEST/FOLDER') == ['database.yml']
    assert os.listdir('TEST/FOLDER2') == ['database.yml']

    remove_files_folders('TEST/FOLDER2')
    assert sorted(os.listdir('TEST')) == sorted(['empty_workflow.py', 'FOLDER'])
    remove_files_folders('TEST/FOLDER/database') # should not do anything
    assert os.listdir('TEST/FOLDER') == ['database.yml']
    remove_files_folders('TEST/FOLDER/database.yml')
    assert os.listdir('TEST/FOLDER') == []
    remove_files_folders('TEST/empty_workflow.py')
    assert os.listdir('TEST') == ['FOLDER']
    remove_files_folders('TEST')
    assert 'TEST' not in os.listdir()

    os.chdir(current_dir)
