"""Test of file generation scripts."""

from collections import namedtuple
from lemmings.base.machine import (convert, 
                                        template_parser,
                                        get_machine_template, get_machine_cmd)
from lemmings.base.user import get_user_params


def test_convert():

    dict_ = {"key1": 0,
             "key2": {
                "key3": "value",
                "key4": 1
             }}
    NamedTupleJJHstyle = namedtuple('NamedTupleJJHstyle', ["key1", "key2"])
    tuple_test = NamedTupleJJHstyle(0,
                             {"key3": "value", "key4": 1})

    tuple_ = convert(dict_)

    assert tuple_ == tuple_test


def test_get_user_params(datadir):

    path_user_params = datadir.join("user_param.yml")

    user = get_user_params(path_user_params)

    assert user.exec == ('executable\n')
    assert user.job_queue == 'debug'
    assert user.pjob_queue == 'debug_pj'
    assert user.job_script_path == 'path/script.py'


#-------from here on it's not unit testing any more as different--# 
#-------function calls are required...----------------------------#
def test_template_parser(datadir):
    """ Checking that the parser replaces the desired strings correctly
        and only if keyword matches what we want.
    """
    job_name = 'test'
    pjob_name = 'test_pj'
    wall_time = '00:20:00'
    path_user_params = datadir.join("user_param.yml")
    user_tuple = get_user_params(path_user_params)
    
    content = template_parser(user_tuple.exec,
                                user_tuple.exec_pj,
                                {'header': '#BATCH\n-EXEC-\n'},
                                job_name)

    assert content == '#BATCH\nexecutable\n\n\n'

    content = template_parser(user_tuple.exec,
                                user_tuple.exec_pj,
                                {'header': '#BATCHPJ\n'},
                                job_name)
                                
    assert content == '#BATCHPJ\n\n'

    content = template_parser(user_tuple.exec,
                                user_tuple.exec_pj,
                                {'header': '#SBATCH --job-name -LEMMING-JOB_NAME-\n'},
                                job_name)
                                
    assert content == '#SBATCH --job-name '+job_name+'\n\n'

    content = template_parser(user_tuple.exec,
                                user_tuple.exec_pj,
                                {'wall_time': wall_time, 'header': '#SBATCH  --time=-LEMMING-WALL-TIME-\n'},
                                job_name)
                                
    assert content == '#SBATCH  --time='+wall_time+'\n\n'

    content = template_parser(user_tuple.exec,
                                user_tuple.exec_pj,
                                {'header': '#SBATCH --job-name -LEMMING-POSTJOB_NAME-\n'},
                                job_name)
                                
    assert content == '#SBATCH --job-name '+pjob_name+'\n\n'


def test_get_machine_template(datadir):

    path_user_params = datadir.join("user_param.yml")
    machine_temp_path = datadir.join("nemo.yml")
    user = get_user_params(path_user_params)


    cmd = get_machine_cmd(machine_temp_path)
    job_template, pj_template = get_machine_template(
        user,
        machine_temp_path,
        'test')

    NamedTupleJJHstyle = namedtuple('NamedTupleJJHstyle', ['submit', 'get_cpu_time', 'dependency'])
    cmd_tgt = NamedTupleJJHstyle('sbatch',
                           'Elapsed',
                           'cmd slurm')
    assert cmd == cmd_tgt

    NamedTupleJJHstyle = namedtuple('NamedTupleJJHstyle', ['wall_time', 'core_nb', 'header', 'batch'])
    job_template_tgt = NamedTupleJJHstyle('00:20:00',
                           24,
                           '#BATCH\n-EXEC-\n',
                           '#BATCH\nexecutable\n\n\n')
    assert job_template == job_template_tgt


    NamedTupleJJHstyle = namedtuple('NamedTupleJJHstyle', ['wall_time', 'header', 'batch'])
    pj_template_tgt = NamedTupleJJHstyle('00:02:00',
                                  '\n#BATCHPJ\n',
                                  '\n#BATCHPJ\n\n')

    assert pj_template == pj_template_tgt

