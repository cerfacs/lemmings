""" Test of the LemmingJobBase class and LemmingsStop
"""
import os

# from datetime import datetime
# import glob

from lemmings.chain.lemmingjob_base import LemmingJobBase
from lemmings.base.database import Database
from lemmings.base.machine import Machine, get_machine_template, get_machine_cmd
from lemmings.base.user import get_user_params


def test_init(datadir):
    """Checking all works well depending on inputs"""
    current_dir = os.getcwd()
    os.chdir(datadir)

    chain_name = "preccJJ_NEDE78"
    job_prefix = "prefix"
    workflow = "sandcastle"
    yml_path = "sandcastle.yml"
    machine_path = "sandbox.yml"
    database = Database()
    user = get_user_params(yml_path)
    cmd = get_machine_cmd(machine_path)
    job_template, pj_template = get_machine_template(user, machine_path, job_prefix)
    machine = Machine(cmd, job_template, pj_template, chain_name, machine_path)
    lemmings_job = LemmingJobBase(
        workflow, machine, database, yml_path, job_prefix, user, 1
    )
    assert lemmings_job.database.latest_chain_name == chain_name
    assert "loop_count" in lemmings_job.database._database[chain_name][0].keys()

    os.chdir(current_dir)


def test_methods(datadir):
    """Check all methods exist and test if testable"""
    current_dir = os.getcwd()
    os.chdir(datadir)
    database = Database()
    chain_name = "preccJJ_NEDE78"
    job_prefix = "prefix"
    yml_path = "sandcastle.yml"
    machine_path = "sandbox.yml"
    user = get_user_params(yml_path)
    cmd = get_machine_cmd(machine_path)
    job_template, pj_template = get_machine_template(user, machine_path, job_prefix)
    machine = Machine(cmd, job_template, pj_template, chain_name, machine_path)
    lemmings_job = LemmingJobBase(
        "fake_workflow",
        machine,
        database,
        "fake_yamlPath",
        job_prefix,
        "fake_user",
        1,
    )
    base_methods = [
        "prior_to_job",
        "abort_on_start",
        "prepare_run",
        "prior_to_new_iteration",
        "after_end_job",
        "check_on_start",
        "check_on_end",
    ]

    attr = dir(lemmings_job)
    for method in base_methods:
        assert method in attr
        assert callable(eval("lemmings_job." + method)) == True

    assert lemmings_job.check_on_start() == True
    assert lemmings_job.check_on_end() == True

    os.chdir(current_dir)
