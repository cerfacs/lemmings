"""
Lemmings script for tutorial with twicer
"""
# Standard python libraries import
import os
from glob import glob
from pathlib import PurePath
import yaml
from lemmings.chain.lemmingjob_base import LemmingJobBase

class LemmingJob(LemmingJobBase):
    """
    A lemming job follows always the same pattern.

              +--------------+ True   +-----------+                     +------------+True +-------------+
    Start----->Check on Start+------->|Prepare Run+--->Job sumission---->Check on end+----->After End Job+------>Happy
              +-----+--------+    ^   +-----------+                     +------+-----+     +-------------+        End
                    |             |                                            |
                    |False        |                                            |False
                    |             |                   +-------------+          |
                                  |                   |  Prior to   |          |
                  Early           +-------------------+new iteration<----------+
                   End                                +-------------+
 
    """

    def check_on_start(self):
        """
        Verify if the condition is already satisfied before launching a lemmings chain.
        """
        limit = self.user.custom_params["simulation_end_time"]
        completion = my_completion(limit)
        if completion < 1.:
            return True
        else:
            print("This workflow already reached completion!")
            return False


    # def prior_to_job(self):
    #     """
    #     *Function that prepares the run when the user launch the Lemmings command.*
    #     """
    #     pass

    # def abort_on_start(self):
    #     """
    #     *What lemmings does if the criterion is reached in the first loop.*
    #     """
    #     pass

    def prepare_run(self):
        """
        Refresh the input file
        """
        last_id = last_folder_id()

        input_twicer =  {
            "init_sol": _name_folder(last_id)+ "/twicer_sol.yml",
            "out_path": _name_folder(last_id+1)
        }
        with open("./twicer_in.yml", "w") as fout:
            yaml.dump(input_twicer,fout)

    # def prior_to_new_iteration(self):
    #     """
    #     Correct the run.params and update status to "spawn_job"
    #     """
    #     pass

    # def after_end_job(self):
    #     """
    #     Exit the lemmings loop and restore the original run.params
    #     """
    #     pass

    def check_on_end(self):
        """
        What to do after post job
        """
        limit = self.user.custom_params["simulation_end_time"]
        completion = my_completion(limit)
        self.set_progress_var(completion)
        if completion < 1.:
            return False
        else:
            return True


# Small helper functions

def my_completion(simtime_target):
    """Evaluation my completion of the workflow"""
    last_id = last_folder_id()
    with open(_name_folder(last_id)+ "/twicer_sol.yml", "r") as fin:
        sol = yaml.load(fin, Loader=yaml.SafeLoader)

    return sol["time"]/simtime_target
    
def last_folder_id(wdir = None):
    """Find the ID of the last folder"""
    if wdir is None:
        wdir = os.getcwd()
    folds=glob("*/twicer_sol.yml")
    max_id = 0
    for fold in folds:
        path_ = PurePath(fold)
        max_id = max(_folder_id(path_),max_id)

    return max_id

def _name_folder(id):
    """Return a  folder name from its integer id
    
    example : 55 -> TEMP_0055 """

    return f"TEMP_{id:04d}"

def _folder_id(folder_):
    """Return a integer id from its folder name
    
    example :  TEMP_0055 -> 55"""
    path_ = PurePath(folder_)
    id_ = int(str(path_.parents[0]).split("_")[-1])
    return id_
