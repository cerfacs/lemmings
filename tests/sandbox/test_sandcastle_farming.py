import os
import subprocess
from glob import glob
import yaml


DURATION = 25
FREQUENCY = 0.1

ANALYSIS_BATCH = """
lemmings-farming status -l logging
lem_sandbox qstat -l logging
cat */*.o
cat */*.e
cat */*.log

"""


def test_sandcastle_farming(datadir):
    os.chdir(datadir)
    start_process = subprocess.Popen(
        f"lem_sandbox start -d {DURATION} --frequency {FREQUENCY} --log_mode logging",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )
    submit_process = subprocess.run(
        "export LEMMINGS_MACHINE='None' ; lemmings-farming run --machine-file sandbox.yml --job-prefix twicer --log_mode logging --inputfile sandcastle_farm.yml sandcastle_farm",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )

    try:
        outs, errs = start_process.communicate(timeout=DURATION + 1)
    except subprocess.TimeoutExpired:
        start_process.kill()
    outs, errs = start_process.communicate()

    print("1########")
    print(outs)
    print(errs)

    print("2########")
    print(submit_process.stdout)
    print(submit_process.stderr)

    subp3 = subprocess.run(
        batch_as_one_line(ANALYSIS_BATCH),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )
    print("3########")
    print(subp3.stdout)
    print(subp3.stderr)

    assert sorted(glob("WF_*")) == [
        "WF_000_twicer",
        "WF_001_twicer",
        "WF_002_twicer",
        "WF_003_twicer",
        "WF_004_twicer",
    ]

    with open("WF_002_twicer/TEMP_0002/twicer_sol.yml", "r") as fin:
        test_dat = yaml.load(fin, Loader=yaml.SafeLoader)
    assert test_dat == {"data": [1.0, 5.0, 7.0, 3.0], "time": 3.0}

    with open("WF_003_twicer/TEMP_0002/twicer_sol.yml", "r") as fin:
        test_dat = yaml.load(fin, Loader=yaml.SafeLoader)
    assert test_dat == {"data": [9.0, 45.0, 63.0, 27.0], "time": 3.0}


def batch_as_one_line(batch_str):
    batch_actions = []
    for line in batch_str.split("\n"):
        if line.startswith("#!"):
            pass  # remove shebang
        elif line.startswith("#SBX"):
            pass  # remove sandbox intrisic params
        elif line.strip() == "":
            pass  # remove blank lines
        else:
            batch_actions.append(line.replace("\n", ""))

    return " ; ".join(batch_actions)
