import os
import subprocess
from glob import glob


DURATION = 25
FREQUENCY = 0.1

ANALYSIS_BATCH = """
lemmings status -l logging
lem_sandbox qstat -l logging
cat *.o
cat *.e
"""


def test_sandcastle(datadir):
    os.chdir(datadir)
    start_process = subprocess.Popen(
        f"lem_sandbox start -d {DURATION} --frequency {FREQUENCY} --log_mode logging",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )

    submit_process = subprocess.run(
        "export LEMMINGS_MACHINE='None' ; lemmings run --machine-file sandbox.yml --log_mode logging --inputfile sandcastle.yml sandcastle",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )

    try:
        outs, errs = start_process.communicate(timeout=DURATION + 1)
    except subprocess.TimeoutExpired:
        start_process.kill()
    outs, errs = start_process.communicate()

    print("1########")
    print(outs)
    print(errs)

    print("2########")
    print(submit_process.stdout)
    print(submit_process.stderr)

    subp3 = subprocess.run(
        batch_as_one_line(ANALYSIS_BATCH),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )
    print("3########")
    print(subp3.stdout)
    print(subp3.stderr)

    assert sorted(glob("TEMP_*")) == ["TEMP_0001", "TEMP_0002", "TEMP_0003"]


def test_sandcastle_canceled(datadir):
    os.chdir(datadir)

    clean_process = subprocess.run(
        f"rm -rf TEMP_0002 TEMP_0003  *.log && lemmings clean -l logging",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )

    start_process = subprocess.Popen(
        f"lem_sandbox start -d {DURATION} --frequency {FREQUENCY} --log_mode logging",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )

    submit_process = subprocess.run(
        "lemmings run --machine-file sandbox.yml --log_mode logging --inputfile sandcastle.yml sandcastle",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )

    end_process = subprocess.run(
        "lemmings status -l logging && lemmings kill --machine-file sandbox.yml --log_mode logging && lemmings status -l logging",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )

    try:
        outs, errs = start_process.communicate(timeout=DURATION + 1)
    except subprocess.TimeoutExpired:
        start_process.kill()
    outs, errs = start_process.communicate()

    checks = {
        "Lemmings Clean stdout": clean_process.stdout,
        "Lemmings Clean stderr": clean_process.stdout,
        "Lemmings Run stdout": submit_process.stdout,
        "LemmingsRun stderr": submit_process.stderr,
        "Status and outs": end_process.stderr,
        "Kill stderr": end_process.stderr,
        "Kill stdout": end_process.stderr,
    }

    subp3 = subprocess.run(
        batch_as_one_line(ANALYSIS_BATCH),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
        shell=True,
    )
    print("3########")
    print(subp3.stdout)
    print(subp3.stderr)

    assert sorted(glob("TEMP_*")) == ["TEMP_0001", "TEMP_0002", "TEMP_0003"]


def batch_as_one_line(batch_str):
    batch_actions = []
    for line in batch_str.split("\n"):
        if line.startswith("#!"):
            pass  # remove shebang
        elif line.startswith("#SBX"):
            pass  # remove sandbox intrisic params
        elif line.strip() == "":
            pass  # remove blank lines
        else:
            batch_actions.append(line.replace("\n", ""))

    return " ; ".join(batch_actions)
