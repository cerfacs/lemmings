
## Coming soon

Here are the next goodies we want to add.

### Refactoring of `PathTools`

`PathTools` is a low level class of lemmings handling the file operations.
It should be simplified using PathLib.

Beware, PathTools depends on the working dir provided, testing it once or twice is far from a complete test.

### Farming :  use symbolic links for folder replication

The farming procedure duplicates the content of the the base folder. This will obviously become a problem if large files are present. Today we encourage the use of external reference files with absolute path in the input files.
However some users can do differently, using symlinks when they do this manually. This should be the default behavior.

This action must be done after the refactoring of path tools : indeed understanding all the edge cases PathTools can cope with is clearly not trivial.

### File lock in the sandbox

The sandbox could use a file lock system for a safer usage. A small effort to do.

### A dumber Lemmings clean

Lemmings clean is purging only a part of the database. It should be more thorough and wipe out all the database.

### Tests on sandbox itself, clean and kill

The 30% of uncovered code are sandbox , clean and kill. And tests are a damn good idea.
