
## Lemmings commands

A list of useful commands are given below. 
Note that `lemmings`and `lemmings-farming` have the same options and concept.
Difference is:
- `lemmings` can start, monitor, and cancel a run in a folder, behaving like a human, without moving around files nor erasing log files.
- `lemmings-farming` can start, monitor, and cancel all the runs in its subdirectories with a single command.


### Recursive workflows commands

These commands must be used in a recursive workflow folder.

```bash
>lemmings
Usage: lemmings [OPTIONS] COMMAND [ARGS]...

  Package lemmings v0.8.0

  *Lemmings workflow CLI*

Options:
  --help  Show this message and exit.

Commands:
  clean   Clean lemmings run files in current folder
  kill    Kill the current job and pjob of lemmings
  run     This is the command to launch your workflow with Lemmings.
  status  Show the status during runtime
```

### Farming workflows commands

These commands must be used in the root of a farming project.

```bash
>lemmings-farming
Usage: lemmings-farming [OPTIONS] COMMAND [ARGS]...

  Package lemmings v0.8.0

  *Lemmings workflow CLI  -FARMING MODE-*

Options:
  --help  Show this message and exit.

Commands:
  clean   Clean lemmings run files in current folder
  kill    Kills all active Workflows.
  run     Launch farming mode of lemmings 'lemmings-farming run...
  status  Show the status during runtime
```