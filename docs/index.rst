.. _header-n0:

.. documentation master file


.. image:: https://www.raspberrypi.org/app/uploads/2019/06/lemmings-original-large-1574x1080.jpg
  :width: 400
  :alt: Lemmings

*Welcome to the lemmings documentation. The reference to the dumb character of the awesome 1991 game (DMA Design,/Psygnosis) is intentional : HPC workflows of multiple interdependant runs easily produce unnecessary or failed runs. The package lemmings can help ... under the supervision of the user!* 

.. toctree::
    :maxdepth: 4

    readme_copy
    redirect.rst
    changelog_copy
    

.. _header-n3:
Lemmings documentation
======================



* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

