# Changelog

All notable changes to this project will be documented in this file.
The format is based on Keep a Changelog,
and this project adheres to Semantic Versioning.


## [0.8.0] To be released

### Added

 - Sandbox mock job scheduler
 - test suite, including recursive and farming cases
 - tutorials sandcastle and sandcastle-farming
 - accurate Logging files

### Changed

 - progress variable set with LemmingJob.set_progress_var() now (simpler than with a database)
 - check_on_start() is now void, as expected.
 - [MAJOR CHANGE] package is now `lemmings`not `lemmings-hpc`
 - database use an official and efficient `lock` mechanism.

### Fixed

 - Testing is back in CI
 - add dependency to `nob` and `lock`

### Deprecated

 - CLI lemmings_hpc deprecated
 - restart option replaced by disc based workflows


## [0.7.0] 2022 / 5 / 13
### Added
 [ - ]
### Changed
 - plenty of cleaning
 - move to setup.cfg

### Fixed
 - corrections on avbp_recursive

### Deprecated
 [ - ]




## [0.6.0] 2021 / 10 / 4
### Added
 -  lemmings-farming with custom machine by user in workflow.yml
 - setup.cfg
 - versioning
### Changed
 - change --user-machine to --machine-file
 - change --custom-machine in farming to --enable-queues-from-yaml or -eqfy
 - use job_prefix and add_farming_suffix to control workflow and chain names in farming mode
 - setup.py, cli
### Fixed
 [ - ]
### Deprecated
 [ - ]


## [0.5.0] 2021 / 09 / 3

### Added
 - Option to override job prefix in the farming mode through keyword "_override_job_prefix" in the parameter_array
 - Option to specify --job-prefix for lemmings run which will override the existing one (required for above feature)
 - Option to specify machine path in CLI and act accordingly in lemmings batch_pjob creation
 - Add entry points lemmings and lemmings-farming
 - Add cli status, clean, run and kill for lemmings and lemmings-farming
 - Add Depreciation Warnings to lemmings-hpc status, clean, run and kill
### Changed
 - Update REAMDE with Depreciation Warning and new entry points
### Fixed
 [ - ]
### Deprecated
 - lemmings-hpc run  -- redirect to new cli calls


## [0.4.4] 2021 / 08 / 27

### Added
 - First docs version for ReadTheDocs
 - Link to gitlab and docs in setup.py
 - lemmings-hpc info to get queue info from machine file
 - lemmings-hpc kill for parallel mode
### Changed
 - Update barbatruc example 
 - Update barbatruc tutorial in docs
### Fixed
 [ - ]
### Deprecated
 [ - ]


## [0.4.3] 2021 / 08 / 24

### Added
 - Remove WF folders from latest parallel chain in lemmings-hpc clean
 - Tests for get current status including parallel version
 - PrettyTable version requirement
 - Extra tests get current status for 1st loop cases in database
### Changed
 - Deactivate write_log_file at end as too AVBP related for now 
### Fixed
 - Reactivate TypeError in cli when database is busy
 - Bugfix identification of progress quantity in lemmings status with progress
 - Update analyse_cuts.py script to latest pyavbp functionalities
 - Correct rendering for first loop if running
### Deprecated
 [ - ]


## [0.4.2] 2021 / 08 / 20

### Added
 - possibility for lemmings status with a directory path where default name database.yml will be sought for
 - possibility to output progress for parallel mode in lemmings status
### Changed
 - Parallel workflow names set to WF instead of Workflow 
### Fixed
 - Correct representation of first loop if its the only loop and condition reached
### Deprecated
 [ - ]


## [0.4.1] 2021 / 08 / 20
### Added
 [ - ]
### Changed
 [ - ]
### Fixed
 - Bugfix add empty end message to newly initialised loop after check on end

### Deprecated
 [ - ]

## [0.4.0] 2021 / 08 / 19
### Added
 - lemmings_hpc --restart option to append to latest existing chain
### Changed
 - refactor create_replicate_workflows() splitting in smaller function calls
 - change call to database.initialise_new_loop() and refactor lemmings accordingly
### Fixed
 - tests lemmings farming updated, pyavbp v2 not working due to pyavbp related import
 - Updated different tests to accommodate changes related to --restart option

### Deprecated
 [ - ]


## [0.3.0] 2021 / 07 / 9
 
### Added
 - function create_replicates() in chain.py to generate multiple workflows and run lemmings in parallel
 - call to create_replicates() from CLI
 - activation parallel mode in workflow.yml file
 - add example/test_workflow to check the implementations
 - add yaml path in batch_pjob which allows relative paths
 - nested object functionalities in database
 - max_parallel_workflows optional from workflow.yml for limiting the number of lemming chains submitted
 - add monitor_replicates() function which launches new workflows
 - function to handle exceptions in workflow
 - function to deal with parallel mode in chain
 - option to specify database path in lemmings status
 - separate function call to customise end message in lemmings status
 - function to set the progress quantity for lemmings status
### Changed
 - lemmings status format update
 - get_current_status returns a list of strings that can be accessed by user

### Fixed
 - Lemmings output status adapted in case solut path does not change
 - run check_on_end() even after cpu condition reached to update database
 - update previous existing tests to current version
 - Update avbp_trappedvtx example
 - README.md lemmings to lemmings_hpc 

### Deprecated
 [ - ]

## [0.2.3] 2021 / 05 / 7
 
### Added
 - Prep for first PyPI release 
 - lemmings status TypeError handling when database.yml file being edited by other program

### Changed
 - change of lemmings to lemmings-hpc with src/lemmings_hpc due to PyPi
 - update README

### Fixed
 [ - ]

### Deprecated
 [ - ]

## [0.2.2] 2021 / 04 / 27
 
### Added
 - lemmings clean CLI command 
### Changed
 [ - ]

### Fixed
 [ - ]

### Deprecated
 [ - ]

## [0.2.1] 2021 / 04 / 14
 
### Added
 [ - ]
### Changed
 [ - ]

### Fixed
  - Handle CPU condition reached scenario with ending message and after end job method 

### Deprecated
 [ - ]

## [0.2.0] 2021 / 04 / 13
 
### Added
 - update CLI for user defined .yml path
 - raise ValueError if file is empty
 - handle AttributeError is cpu_limit not defined in .yml file
 - example avbp trappedvtx
 - example avbp trappedvtx with postproc coupled and decoupled
 - create batch files as part of lemmings with option to be user defined
 - LemmingsStop class to safely exit lemmings on exceptions
 - Default methods as part of lemmingjob_base which can be overwritten by user defined once
 - Add lemmings status for during job run
 - check in Machine class that core_nb specified by the user
 - end messages in database across chain that can be output by lemmings status

### Changed
 - Update README.md
 - move example folder to main directory
 - update docs barbatruc tutorial
 - updated exception handling for .yml and .py workflow files
 - handling crash and database upon check_on_end()
 - remove tekigo workflows
 - remove lemmings info CLI option for now
 - remove workflows directory with default workflows
 - moved current lemmings status to lemmings endlog
 - Update get_cpu_cost in machine.py allowing to handle different return formats
 - remove 'lemmings init' command

### Fixed
 [ - ]

### Deprecated
 [ - ]

## [0.1.0] 2020 / 07 / 01
 
 
### Added
 - New folder created with the chain name as name:
 
 	- .log file with some parameters of each loop
 	- avbp log files 
 	- run.params when user launch lemmings
 
 - New documentation in the README file
 - New argument in the wf configuration file : solution\_writing\_time
 - New argument in the wf configuration file : job_prefix (put a prefix before the auto generate chain name)
	
### Changed
 - Lemmings run command raise an error if the workflow contains an extension (avbp_recusrif{.yml})

### Fixed
 [ - ]

### Deprecated
 [ - ]


## [0.0.1] 2020 / 06 / 20
 
First version of Lemmings, a job schedulers wrapper

- workflow: avbp\_recursif, avbp\_p40, avbp\_outilbest 

- lemmings CLI
	
