test:
	PYTHONDONTWRITEBYTECODE=1 py.test tests --cov=lemmings -v -x 

test_html:
	PYTHONDONTWRITEBYTECODE=1 py.test tests --cov=lemmings --cov-report html


init:
	pip install -r requirements.txt

lint:
	pylint src/lemmings

wheel:
	rm -rf build
	rm -rf dist
	python setup.py sdist bdist_wheel

upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

upload:
	twine upload dist/*

.PHONY: init
